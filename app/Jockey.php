<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Jockey extends Model {
    protected $guarded = array('id');
}
