<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use Input;
use App\Meeting;
use App\Race;

class MeetingsController extends APIController {
    protected $model = 'App\Meeting';
    protected $duplicate_key = array(
        'datevalue'
    );

    public function next()
    {
        $next = new Meeting;
        $next = $next->where('number_of_races', '=', 0)
            ->where('datevalue', 'LIKE', 'Local%')
            ->where('datevalue', 'NOT LIKE', 'Local/'.date('Ymd').'%') //exclude today
            ->orderBy('datevalue')
            ->get();

        return $next;
    }

    public function completed()
    {
        $input = array_except(Input::all(), '_method');
        $datevalue = json_decode($input['datevalue'], true);
        $m = new Meeting;
        $m = $m->where('datevalue', '=', $input['datevalue'])
            ->take(1)
            ->get();

        $meeting_id = null;
        $count = 0;
        $r = array();
        if(count($m) > 0){
            $meeting_id = $m[0]['id'];
            $race = new Race;
            $count = $race->where('meeting_id', '=', $meeting_id)
                ->get()
                ->count();
            if($count > 0){
                $m[0]->number_of_races = $count;
                $m[0]->save();
            }
        }
        $r['meeting_id'] = $meeting_id;
        $r['count'] = $count;

        return $r;
    }
}
