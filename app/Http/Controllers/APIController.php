<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Input;

class APIController extends Controller {

    protected $model = '';
    protected $duplicate_key = array(); //define which columns are unique key, it should be set in Model though

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return call_user_func($this->model.'::all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = array_except(Input::all(), '_method');

        $row = array();
        if(count($this->duplicate_key) > 0){
            $duplicate_key = array();
            foreach($this->duplicate_key as $i => $j){
                if(isset($input[$j])){
                    $duplicate_key[$j] = $input[$j];
                }
            }
            $row = call_user_func($this->model.'::updateOrCreate', $duplicate_key, $input);
        }else{
            $row = call_user_func($this->model.'::create', $input);
        }
        return $row;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $m = call_user_func($this->model.'::find', $id);
        $input = array_except(Input::all(), '_method');
        $m->update($input);

        return $m;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        call_user_func($this->model.'::destroy', $id);
    }

}
