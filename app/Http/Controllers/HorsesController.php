<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Horse;

class HorsesController extends APIController {
    protected $model = 'App\Horse';
    protected $duplicate_key = array(
        'code',
    );
}
