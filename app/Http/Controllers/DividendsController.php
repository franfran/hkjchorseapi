<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Dividend;

class DividendsController extends APIController {
    protected $model = 'App\Dividend';
    protected $duplicate_key = array(
        'race_id',
        'pool',
        'winning_combination'
    );
    public function race($race_id)
    {
        $d = new Dividend;
        $d = $d->where('race_id', '=', $race_id)
            ->get();

        return $d;
    }
}
