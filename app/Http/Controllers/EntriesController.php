<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Entry;
use App\Meeting;
use Input;
use Route;
use Request;

class EntriesController extends APIController {
    protected $model = 'App\Entry';
    protected $duplicate_key = array(
        'meeting_date',
        'race_number',
        'number',
    );

    public function next()
    {
        $next = new Meeting;
        $next = $next->where('entry', '=', 0)
            ->where(function($query){
                $query->orWhere('datevalue', 'LIKE', 'Local%ST%')
                ->orWhere('datevalue', 'LIKE', 'Local%HV%');
            })
            ->orderBy('datevalue')
            ->get();

        return $next;
    }

    public function completed()
    {
        $input = array_except(Input::all(), '_method');
        $datevalue = json_decode($input['datevalue'], true);
        $m = new Meeting;
        $m = $m->where('datevalue', '=', $input['datevalue'])
            ->take(1)
            ->get();
        $meeting_id = null;
        $r = array();
        if(count($m) > 0){
            $meeting_id = $m[0]['id'];
            $m[0]->entry = 1;
            $m[0]->save();
        }
        $r['meeting_id'] = $meeting_id;

        return $r;
    }

    public function store()
    {
        $input = array_except(Input::all(), '_method');

        //entry could be updated due to win_odds changes, horse canceled from race
        if(isset($input['meeting_date']) && isset($input['race_number']) && isset($input['number'])){
            //determine the entry id
            $entry = new Entry;
            $entry = $entry->where('meeting_date', '=', $input['meeting_date'])
                ->where('race_number', '=', $input['race_number'])
                ->where('number', '=', $input['number'])
                ->take(1)
                ->get();
            if(count($entry) > 0){
                $r = $entry[0];
                $input['id'] = $r->id;
                Input::replace($input);
            }
        }

        return parent::store();
    }

    private function get_horse($i){
        $id = null;
        $data = array();
        $data['code'] = @$i['horse_code'];
        $data['name'] = @$i['horse'];
        if(!empty($data['code']) && !empty($data['name'])){
            $request = Request::create("/api/horses", "POST", $data);
            Request::replace($request->input());
            $object = json_decode(Route::dispatch($request)->getContent(), true);
            $id = @$object['id'];
        }

        return $id;
    }

    private function get_jockey($i){
        $id = null;
        $data = array();
        $data['code'] = @$i['jockey_code'];
        $data['name'] = @$i['jockey'];
        if(!empty($data['code']) && !empty($data['name'])){
            $request = Request::create("/api/jockeys", "POST", $data);
            Request::replace($request->input());
            $object = json_decode(Route::dispatch($request)->getContent(), true);
            $id = @$object['id'];
        }

        return $id;
    }

    private function get_trainer($i){
        $id = null;
        $data = array();
        $data['code'] = @$i['trainer_code'];
        $data['name'] = @$i['trainer'];
        if(!empty($data['code']) && !empty($data['name'])){
            $request = Request::create("/api/trainers", "POST", $data);
            Request::replace($request->input());
            $object = json_decode(Route::dispatch($request)->getContent(), true);
            $id = @$object['id'];
        }

        return $id;
    }

    /*
     * Allow to post race basic_info, entries
     * and update the related tables all at once
     */
    public function scraper_upload()
    {
        $input = array_except(Input::all(), '_method');
        $data = json_decode($input['data'], true);
        $basic_info = $data['basic_info'];
        $entries = $data['entries'];
        $entry_id = array();

        //update the entries
        foreach($entries as $i){
            $horse_id = $this->get_horse($i);
            $jockey_id = $this->get_jockey($i);
            $trainer_id = $this->get_trainer($i);

            if(!empty($horse_id) && !empty($jockey_id) && !empty($trainer_id)){
                //re-map fields to db fields
                $i = array_merge($i, $basic_info);
                unset($i['horse_code']);
                unset($i['jockey_code']);
                unset($i['trainer_code']);
                $i['horse_id'] = $horse_id;
                $i['jockey_id'] = $jockey_id;
                $i['trainer_id'] = $trainer_id;
                $i['horse_name'] = $i['horse'];
                $i['jockey_name'] = $i['jockey'];
                $i['trainer_name'] = $i['trainer'];
                unset($i['horse']);
                unset($i['jockey']);
                unset($i['trainer']);
                $i['actual_weight'] = str_replace(',', '', $i['actual_weight']);
                $i['declared_weight'] = str_replace(',', '', $i['declared_weight']);
                if(isset($i['win_odds'])){
                    $i['win_odds'] = str_replace(',', '', $i['win_odds']);
                }
                $request = Request::create("/api/entries", "POST", $i);
                Request::replace($request->input());
                $r = json_decode(Route::dispatch($request)->getContent(), true);
                if(isset($r['id'])){
                    $entry_id[] = $r['id'];
                }
            }
        }

        $return = array();
        $return['entries'] = $entry_id;

        return $return;
    }

}
