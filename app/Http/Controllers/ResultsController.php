<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Result;

class ResultsController extends APIController {
    protected $model = 'App\Result';
    protected $duplicate_key = array(
        'race_id',
        'horse_id'
    );
}
