<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Jockey;

class JockeysController extends APIController {
    protected $model = 'App\Jockey';
    protected $duplicate_key = array(
        'code',
    );
}
