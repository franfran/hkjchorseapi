<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Race;
use App\Meeting;
use Input;
use Route;
use Request;

class RacesController extends APIController {
    protected $model = 'App\Race';
    protected $duplicate_key = array(
        'meeting_id',
        'race_number'
    );

    public function store()
    {
        $input = array_except(Input::all(), '_method');

        if(isset($input['meeting'])){
            //determine the meeting_id
            $meeting = new Meeting;
            $meeting = $meeting->where('datevalue', '=', $input['meeting'])
                ->take(1)
                ->get();
            if(count($meeting) > 0){
                $r = $meeting[0];
                unset($input['meeting']);
                $input['meeting_id'] = $r->id;
                Input::replace($input);
                return parent::store();
            }
        }
    }

    private function get_horse($i){
        $id = null;
        $data = array();
        $data['code'] = @$i['horse_code'];
        $data['name'] = @$i['horse'];
        if(!empty($data['code']) && !empty($data['name'])){
            $request = Request::create("/api/horses", "POST", $data);
            Request::replace($request->input());
            $object = json_decode(Route::dispatch($request)->getContent(), true);
            $id = @$object['id'];
        }

        return $id;
    }

    private function get_jockey($i){
        $id = null;
        $data = array();
        $data['code'] = @$i['jockey_code'];
        $data['name'] = @$i['jockey'];
        if(!empty($data['code']) && !empty($data['name'])){
            $request = Request::create("/api/jockeys", "POST", $data);
            Request::replace($request->input());
            $object = json_decode(Route::dispatch($request)->getContent(), true);
            $id = @$object['id'];
        }

        return $id;
    }

    private function get_trainer($i){
        $id = null;
        $data = array();
        $data['code'] = @$i['trainer_code'];
        $data['name'] = @$i['trainer'];
        if(!empty($data['code']) && !empty($data['name'])){
            $request = Request::create("/api/trainers", "POST", $data);
            Request::replace($request->input());
            $object = json_decode(Route::dispatch($request)->getContent(), true);
            $id = @$object['id'];
        }

        return $id;
    }

    /*
     * Allow to post race basic_info, results, dividends
     * and update the related tables all at once
     */
    public function scraper_upload()
    {
        $input = array_except(Input::all(), '_method');
        $data = json_decode($input['data'], true);
        $basic_info = $data['basic_info'];
        $results = $data['results'];
        $dividends = $data['dividends'];

        //$originalInput = Request::input();
        //$originalRoute = Route::getCurrentRoute();
        //Request::replace($originalInput);//restore orginal input
        $request = Request::create("/api/races", "POST", $basic_info);//create new request for self API post
        Request::replace($request->input());//overwrite current request variables
        $race = json_decode(Route::dispatch($request)->getContent(), true);//invoke API
        $race_id = @$race['id'];
        $result_id = array();
        $dividend_id = array();

        //update the result
        foreach($results as $i){
            $horse_id = $this->get_horse($i);
            $jockey_id = $this->get_jockey($i);
            $trainer_id = $this->get_trainer($i);

            if(!empty($race_id) && !empty($horse_id) && !empty($jockey_id) && !empty($trainer_id)){
                unset($i['horse_code']);
                unset($i['horse']);
                unset($i['jockey_code']);
                unset($i['jockey']);
                unset($i['trainer_code']);
                unset($i['trainer']);
                $i['race_id'] = $race_id;
                $i['horse_id'] = $horse_id;
                $i['jockey_id'] = $jockey_id;
                $i['trainer_id'] = $trainer_id;
                $i['actual_weight'] = str_replace(',', '', $i['actual_weight']);
                $i['declared_weight'] = str_replace(',', '', $i['declared_weight']);
                $i['win_odds'] = str_replace(',', '', $i['win_odds']);
                $request = Request::create("/api/results", "POST", $i);
                Request::replace($request->input());
                $r = json_decode(Route::dispatch($request)->getContent(), true);
                if(isset($r['id'])){
                    $result_id[] = $r['id'];
                }
            }
        }

        //update the dividend
        if(!empty($race_id)){
            foreach($dividends as $i){
                $i['race_id'] = $race_id;
                $i['dividend'] = str_replace(',', '', $i['dividend']);
                $request = Request::create("/api/dividends", "POST", $i);
                Request::replace($request->input());
                $r = json_decode(Route::dispatch($request)->getContent(), true);
                if(isset($r['id'])){
                    $dividend_id[] = $r['id'];
                }
            }
        }

        $return = array();
        $return['race'] = $race_id;
        $return['results'] = $result_id;
        $return['dividends'] = $dividend_id;

        return $return;
    }

}
