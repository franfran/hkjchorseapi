<?php namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Trainer;

class TrainersController extends APIController {
    protected $model = 'App\Trainer';
    protected $duplicate_key = array(
        'code',
    );
}
