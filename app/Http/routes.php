<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('api/meetings/next', 'MeetingsController@next');
Route::resource('api/meetings/completed', 'MeetingsController@completed');
Route::resource('api/races/scraper_upload', 'RacesController@scraper_upload');
Route::get('api/dividends/race/{race_id}', 'DividendsController@race');
Route::resource('api/entries/next', 'EntriesController@next');
Route::resource('api/entries/completed', 'EntriesController@completed');
Route::resource('api/entries/scraper_upload', 'EntriesController@scraper_upload');

Route::resource('api/dividends', 'DividendsController');
Route::resource('api/horses', 'HorsesController');
Route::resource('api/jockeys', 'JockeysController');
Route::resource('api/meetings', 'MeetingsController');
Route::resource('api/races', 'RacesController');
Route::resource('api/results', 'ResultsController');
Route::resource('api/trainers', 'TrainersController');
Route::resource('api/entries', 'EntriesController');
