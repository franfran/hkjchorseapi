<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorsesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('horses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('country_of_origin')->nullable();
			$table->string('color')->nullable();
			$table->string('sex')->nullable();
			$table->string('owner')->nullable();
			$table->string('import_type')->nullable();
			$table->string('sire')->nullable();
			$table->string('dam')->nullable();
			$table->string('dam_sire', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('horses');
	}

}
