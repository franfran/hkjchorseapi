<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('results', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('race_id')->unsigned();
			$table->boolean('place')->nullable();
			$table->boolean('number');
			$table->integer('horse_id')->unsigned();
			$table->integer('jockey_id')->unsigned();
			$table->integer('trainer_id')->unsigned()->nullable();
			$table->smallInteger('actual_weight')->unsigned()->nullable();
			$table->smallInteger('declared_weight')->unsigned()->nullable();
			$table->boolean('draw');
			$table->string('left_behind_winner')->nullable();
			$table->string('running_position')->nullable();
			$table->string('finish_time')->nullable();
			$table->decimal('win_odds', 12)->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('results');
	}

}
