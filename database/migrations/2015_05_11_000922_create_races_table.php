<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('races', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('meeting_id')->unsigned()->nullable();
			$table->boolean('race_number');
			$table->boolean('number_of_horses')->nullable();
			$table->string('horses_class')->nullable();
			$table->string('distance')->nullable();
			$table->string('prize')->nullable();
			$table->string('going')->nullable();
			$table->string('course')->nullable();
			$table->string('sectional_time')->nullable();
			$table->string('location')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('races');
	}

}
