<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDividendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dividends', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('race_id')->unsigned()->nullable();
			$table->string('pool')->nullable();
			$table->string('winning_combination')->nullable();
			$table->decimal('dividend', 12)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dividends');
	}

}
